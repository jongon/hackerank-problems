var numbers = [20, 23, 6]

function reverse_number(number) {
    number = number + ""
    return parseInt(number.split('').reverse().join(''))
}

var beautyTimes = 0;
for (var i = numbers[0]; i <= numbers[1]; i++) {
    var absoluteDiff = Math.abs(i - reverse_number(i))
    if (absoluteDiff % numbers[2] == 0)
        beautyTimes++
}

console.log(beautyTimes)