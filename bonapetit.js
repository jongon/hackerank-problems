function processData(input) {
    //Enter your code here
    var lines = input.split("\n")
    var firstLine = lines[0].split(' ')
    var secondLine = lines[1].split(' ')
    var thirdLine = lines[2]
    var items = firstLine[0]
    var k = firstLine[1]
    var plates = secondLine
    var charged = thirdLine

    var realCharge = 0
    for (var i = 0; i < items.length; i++) {
        if (k != i)
            realCharge += plates[i]
    }
    realCharge = realCharge / 2
    var annaCharge = realCharge - charged
    if (annaCharge == 0) {
        console.log("Bon Appetit")
    } else {
        console.log(annaCharge)
    }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function(input) {
    _input += input;
});

process.stdin.on("end", function() {
    processData(_input);
});