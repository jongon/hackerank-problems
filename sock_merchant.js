var sockNumber = 9
var sockArray = [10, 20, 20, 10, 10, 30, 50, 10, 20]

var match = 0
while (sockArray.length > 1) {
    var firstSock = sockArray[0]
    sockArray.shift()
    for (var i = 0; i < sockArray.length; i++) {
        if (sockArray[i] == firstSock) {
            match++
            sockArray.splice(i, 1)
            break
        }
    }
}

console.log(match)