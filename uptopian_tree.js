var n = 3
var cycles = [0, 1, 4]

var heights = []

for (var i = 0; i < cycles.length; i++) {
    var startHeight = 1
    for (var j = 0; j < cycles[i]; j++) {
        if (j % 2 == 0)
            startHeight *= 2
        else
            startHeight += 1
    }

    heights.push(startHeight)
}

console.log(heights)